<?php
include '../conn/Conn.php';

class DrinkWater extends Conn {
    public function save($input) {
        $sql = "INSERT INTO userdrinkwater (var_name, var_email, var_password) VALUES (:username, :useremail, sha1(:userpassword))";
        
        $conn = Conn::prepare($sql);
        $conn->bindValue('username', $input->name);
        $conn->bindValue('useremail', $input->email);
        $conn->bindValue('userpassword', $input->password);
        return $conn->execute();
    }

    public function edit($input, $token) {
        $sql = "UPDATE userdrinkwater SET var_name = :username, var_email = :useremail, var_password = sha1(:userpassword) WHERE iduser = :userid AND var_token = :usertoken";
        
        $conn = Conn::prepare($sql);
        $conn->bindValue('username', $input->name);
        $conn->bindValue('useremail', $input->email);
        $conn->bindValue('userpassword', $input->password);
        $conn->bindValue('userid', $input->iduser);
        $conn->bindValue('usertoken', $token);
        return $conn->execute();
    }

    public function delete($iduser, $token) {
        $sql = "DELETE FROM userdrinkwater WHERE iduser = :userid AND var_token = :usertoken";
        
        $conn = Conn::prepare($sql);
        $conn->bindValue('usertoken', $token);
        $conn->bindValue('userid', $iduser);
        return $conn->execute();
    }

    public function update($input) {
        $sql = "UPDATE userdrinkwater SET num_drinkcounter = num_drinkcounter+:userdrink WHERE iduser = :userid";

        $conn = Conn::prepare($sql);
        $conn->bindValue('userdrink', $input->drink_ml);
        $conn->bindValue('userid', $input->iduser);
        $conn->execute();

        $userDetails = $this->findById($input->iduser);
        return $userDetails;
    }

    public function findByEmail($email) {
        $sql = "SELECT iduser FROM userdrinkwater WHERE var_email = :useremail";
        $conn = Conn::prepare($sql);
        $conn->bindValue('useremail', $email);
        $conn->execute();
        return $conn->fetch();
    }

    public function userByLogin($input) {
        $sql = "SELECT iduser, var_name, var_email, num_drinkcounter, var_token FROM userdrinkwater WHERE var_email = :useremail AND var_password = sha1(:userpassword)";
        
        $conn = Conn::prepare($sql);
        $conn->bindValue('useremail', $input->email);
        $conn->bindValue('userpassword', $input->password);
        $conn->execute();
        $userDetails = $conn->fetch();
                
        if ($userDetails) {
            $token = __token__($userDetails->iduser, $userDetails->var_email);
            $saveToken = $this->updateToken($token, $userDetails->iduser);            
            if ($saveToken) {
                $userDetails->var_token = $token;
            }
        }

        return $userDetails;
    }

    public function findById($iduser) {
        $sql = "SELECT iduser, var_name, var_email, num_drinkcounter FROM userdrinkwater WHERE iduser = :userid";

        $conn = Conn::prepare($sql);        
        $conn->bindValue('userid', $iduser);
        $conn->execute();
        return $conn->fetch();

    }

    public function findAll() {
        $sql = "SELECT iduser, var_name, var_email, num_drinkcounter FROM userdrinkwater";
        
        $conn = Conn::prepare($sql);
        $conn->execute();
        return $conn->fetchAll();

    }

    public function updateToken($token, $iduser) {
        $sql = "UPDATE userdrinkwater SET var_token = :usertoken WHERE iduser = :userid";

        $conn = Conn::prepare($sql);
        $conn->bindValue('usertoken', $token);
        $conn->bindValue('userid', $iduser);
        return $conn->execute();
    }

    public function validToken($token) {
        $sql = "SELECT iduser FROM userdrinkwater WHERE var_token = :usertoken";
        
        $conn = Conn::prepare($sql);
        $conn->bindValue('usertoken', $token);
        $conn->execute();
        return $conn->fetch();
    }

}