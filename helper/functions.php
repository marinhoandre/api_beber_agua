<?php

function __output_header__( $__success = true, $__message = null, $_data = array() ) {
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(
        array(
            'success' => $__success,
            'message' => $__message,
            'dados'   => $_data
        )
    );
    exit;
}

function __token__($__id = null, $__email = null) {

    $key = '';

    $header =[
        'typ'=>'JWT', 
        'alg'=>'HS256'
    ];
    $content = [
        'exp'=>(new DateTime("now"))->getTimestamp(),
        'uid' => $__id,
        'email' => $__email
    ];

    $header = json_encode($header);
    $content = json_encode($content);

    $header = base64_encode($header);
    $content = base64_encode($content);

    $sign = hash_hmac('sha256',$header.'.'.$content, $key, true);
    $sign = base64_encode($sign);

    $token = $header.'.'.$content.'.'.$sign;

    return $token;

}