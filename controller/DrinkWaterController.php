<?php
include '../model/DrinkWater.php';

class DrinkWaterController {
    

    function save($input) {
        $drinkWater = new DrinkWater();
        return $drinkWater->save($input);
    }

    function userByEmail($email) {
        $drinkWater = new DrinkWater();
        return $drinkWater->findByEmail($email);
    }

    function userByLogin($input) {
        $drinkWater = new DrinkWater();
        return $drinkWater->userByLogin($input);
    }

    function findById($input, $token) {
        $drinkWater = new DrinkWater();

        $returnUser = '';

        if ($drinkWater->validToken($token)) {
            $returnUser = $drinkWater->findById($input->iduser);
        }

        return $returnUser;
    }

    function findAll($token) {
        $drinkWater = new DrinkWater();

        $returnAll = '';
        
        if ($drinkWater->validToken($token)) {
            $returnAll = $drinkWater->findAll();
        }

        return $returnAll;
    }

    function edit($input, $token) {
        $drinkWater = new DrinkWater();
        
        $returnEdit = '';

        if ($drinkWater->validToken($token)) {
            $returnEdit = $drinkWater->edit($input, $token);
        }

        return $returnEdit;
    }

    function delete($iduser, $token) {
        $drinkWater = new DrinkWater();
        
        $returnDelete = '';
        
        if ($drinkWater->validToken($token)) {
            $returnDelete = $drinkWater->delete($iduser, $token);
        }

        return $returnDelete;
    }

    function update($input, $token) {
        $drinkWater = new DrinkWater();

        $returnUpdate = '';
        
        if ($drinkWater->validToken($token)) {
            $returnUpdate = $drinkWater->update($input);
        }

        return $returnUpdate;
    }
}