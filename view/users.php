<?php
include '../helper/functions.php';
include '../controller/DrinkWaterController.php';

$drinkWater = new DrinkWaterController;

$input = file_get_contents('php://input');
$obj =  json_decode($input);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        if (!empty($input)) {            
            if (isset($obj->drink_ml)) {
                if (!empty($_SERVER['HTTP_TOKEN'])) {
                    $URI = explode('/',$_SERVER['REQUEST_URI']);
                    $iduser = $URI[2];                    
                    if($URI[3] == 'drink') {
                        $obj->iduser = $iduser;
                        $returnUpdate = $drinkWater->update($obj, $_SERVER['HTTP_TOKEN']);
                        if ($returnUpdate) {
                            __output_header__( true, "Usuário atualizado com sucesso!", $returnUpdate );
                        } else {
                            __output_header__( false, "Ocorreu um problema ao atualizar o usuário, tente mais tarde!", null );
                        }
                    }
                } else {
                    __output_header__( false, "Token inválido.", null );
                }
            } else{
                $userexist = $drinkWater->userByEmail($obj->email);
                if (!$userexist) {
                    $drinkWater->save($obj);
                    if($drinkWater == 1) {
                        __output_header__( true, "Usuário salvo com sucesso!", null );
                    } else {
                        __output_header__( false, "Ocorreu um problema ao salvar o usuário, tente mais tarde!", null );
                    }        
                } else {
                    __output_header__( false, "Usuário já existe!", null );
                }
            }
        }
        break;
    case 'GET':
        if (!empty($_SERVER['HTTP_TOKEN'])) {
            $URI = explode('/',$_SERVER['REQUEST_URI']);
            $iduser = $URI[2];
            if ($iduser > 0) {
                if (!empty($input)) {    
                    $obj->iduser = $iduser;
                    $findUserById = $drinkWater->findById($obj, $_SERVER['HTTP_TOKEN']);
                    
                    if ($findUserById) {
                        __output_header__( true, "Usuário encontrado!", $findUserById );
                    } else {
                        __output_header__( false, "Usuário não existe!", null );
                    }
                }
            } else {
                $findUserAll = $drinkWater->findAll($_SERVER['HTTP_TOKEN']);
                
                if ($findUserAll) {
                    __output_header__( true, "Usuários encontrado!", $findUserAll );
                } else {
                    __output_header__( false, "Não existe nenhum usuário!", null );
                }
            }
        } else {
            __output_header__( false, "Token inválido.", null );
        }
    
        break;
    case 'PUT':
        $URI = explode('/',$_SERVER['REQUEST_URI']);
        $iduser = $URI[2];

        if (!empty($input)) {
            $obj->iduser = $iduser;
            $drinkWater->edit($obj, $_SERVER['HTTP_TOKEN']);
            
            if($drinkWater == 1) {
                __output_header__( true, "Usuário alterado com sucesso!", null );
            } else {
                __output_header__( false, "Ocorreu um problema ao alterar o usuário, tente mais tarde!", null );
            }
        }
        break;
    case 'DELETE':
        $URI = explode('/',$_SERVER['REQUEST_URI']);
        $iduser = $URI[2];

        $returnDelete = $drinkWater->delete($iduser, $_SERVER['HTTP_TOKEN']);
        die($returnDelete);
        if ($returnDelete) {
            __output_header__( true, "Usuário excluído com sucesso!", null );
        } else {

        }
        break;
    default:
        __output_header__( false, "Método de requisição não aceito.", null );
        break;
}
