<?php
include '../helper/functions.php';
include '../controller/DrinkWaterController.php';

if( $_SERVER['REQUEST_METHOD'] !== "POST" ) {
    __output_header__( false, "Método de requisição não aceito.", null );
} 

$drinkWater = new DrinkWaterController;

$input = file_get_contents('php://input');
$obj =  json_decode($input);

if (!empty($input)) {    
    $userLogin = $drinkWater->userByLogin($obj);

    if ($userLogin) {
        __output_header__( true, "Usuário encontrado!", $userLogin );
    } else {
        __output_header__( false, "Usuário não existe!", null );
    }
}